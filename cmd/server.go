package main

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	yaml "gopkg.in/yaml.v3"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"server/server"
	"server/server/common/db"
	"server/server/common/log"
	"server/server/config"
	"server/server/monitor"
	"syscall"
	"time"
)

const CONFIG_FILE string = "conf/application.yaml"

func main() {

	runtime.GOMAXPROCS(runtime.NumCPU())

	yamlFile, err := ioutil.ReadFile(CONFIG_FILE)
	if err != nil {
		panic("can load " + CONFIG_FILE)
	}

	conf := config.Config
	err1 := yaml.Unmarshal(yamlFile, &conf)
	if err1 != nil {
		panic("parse " + CONFIG_FILE + " error:" + err1.Error())
	}
	config.Config = conf

	level, error := logrus.ParseLevel(conf.Log.Level)
	if error != nil {
		panic("parse log level error" + error.Error())
	}
	log.LoggerInit(conf.Log.Path, conf.Log.File, level)

	gin := gin.Default()
	gin.Use(LoggerToFile(log.Logger()))

	server.Router(gin, &monitor.MonitorInterface)

	ADDRESS := conf.Server.Address
	err2 := gin.Run(ADDRESS)
	if err2 != nil {
		panic("start server at:" + ADDRESS + " error" + err2.Error())
	}

	start0()

	server := &http.Server{
		Addr:         ADDRESS,
		Handler:      gin,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	gracefulExitWeb(server)

}

func LoggerToFile(logger *logrus.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		// 开始时间
		startTime := time.Now()
		// 处理请求
		c.Next()
		// 结束时间
		endTime := time.Now()
		// 执行时间
		latencyTime := endTime.Sub(startTime)
		// 请求方式
		reqMethod := c.Request.Method
		// 请求路由
		reqUri := c.Request.RequestURI
		// 状态码
		statusCode := c.Writer.Status()
		// 请求IP
		clientIP := c.ClientIP()
		// 日志格式
		logger.Infof("| %3d | %13v | %15s | %s | %s |",
			statusCode,
			latencyTime,
			clientIP,
			reqMethod,
			reqUri,
		)
	}
}

func gracefulExitWeb(server *http.Server) {
	ch := make(chan os.Signal)
	signal.Notify(ch, syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGINT)
	sig := <-ch
	log.Error("got a signal", sig)

	now := time.Now()

	cxt, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	func() {
		defer func() {
			if err := recover(); err != nil {
				log.Error("close0 error:", err)
			}
		}()
		close0()
	}()
	err := server.Shutdown(cxt)
	if err != nil {
		log.Error("err", err)
	}

	log.Error("shutdown server cost:", time.Since(now))
}

func start0() {
	db.Start()
}

func close0() {
	db.Close()

}
