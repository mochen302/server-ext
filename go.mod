module server

go 1.12

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	github.com/beevik/ntp v0.3.0
	github.com/cloudflare/tableflip v1.2.0
	github.com/coreos/go-systemd v0.0.0-20190719114852-fd7a80b32e1f // indirect
	github.com/gin-gonic/gin v1.4.0
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/jackc/pgx/v4 v4.6.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/kr/pty v1.1.8 // indirect
	github.com/lib/pq v1.2.0
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/objx v0.2.0 // indirect
	golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7
	golang.org/x/tools v0.0.0-20190823170909-c4a336ef6a2f // indirect
	google.golang.org/appengine v1.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20190502103701-55513cacd4a
)
