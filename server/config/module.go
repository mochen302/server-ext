package config

var Config *ConfigInfo

type ConfigInfo struct {
	Server struct {
		Address string `yaml:"address"`
	}
	Log struct {
		Path  string `yaml:"path"`
		File  string `yaml:"file"`
		Level string `yaml:"level"`
	}
	Monitor struct {
		Cmd []string `yaml:"cmd"`
	}
	LevelAnalysis struct {
		RedshiftUrl string `yaml:"redshift_url"`
	}
}
