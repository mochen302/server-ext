package v1

import (
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"github.com/gin-gonic/gin"
	"net/http"
	"sort"
	"strconv"
	"time"
)

const (
	LAYOUT_YEAR_MONTH_DAY = "2006-01-02"
	LAYOUT                = "2006-01-02-15:04:05"
	SHEET_NAME            = "Sheet1"
	MAX                   = int64(1000000000000)
	OutputFileName        = "level_data_%v_%v_%v_%v.xlsx"
)

func init() {
	fmt.Println("-------init-------")
	go init0()
}

func init0() {
	t := time.NewTicker(time.Second * 3)
	defer t.Stop()

	for {
		<-t.C
		//info, err := loadInfo(100, 101, "2020-04-13", "2020-04-14", "11", "2")
		//fmt.Println(err)
		//bytes, err := json.Marshal(info)
		//fmt.Println(string(bytes))
		break
	}
}

func Load(c *gin.Context) ([]*Info, error) {
	infoList, err := loadInfoInner(c)
	if err != nil {
		return make([]*Info, 0), err
	}
	return infoList, nil
}

func Export(c *gin.Context) (string, error) {
	infoList, err := loadInfoInner(c)
	if err != nil {
		return "", err
	}

	sort.Slice(infoList, func(i, j int) bool {
		leftI := int64(infoList[i].Level_pos_id)
		if leftI < 0 {
			leftI = MAX + -leftI
		}

		rightI := int64(infoList[j].Level_pos_id)
		if rightI < 0 {
			rightI = MAX + -rightI
		}

		return leftI < rightI || leftI == rightI && infoList[i].Level_version > infoList[j].Level_version
	})

	file := generateExcelFile(infoList)
	dateMin, _ := getDate(c, "date_min")
	dateMax, _ := getDate(c, "date_max")
	abtestId := c.Query("abtest_id")
	if abtestId == "" {
		abtestId = "-"
	}

	abtestType := c.Query("abtest_type")
	if abtestType == "" {
		abtestType = "-"
	}

	fileName := fmt.Sprintf(OutputFileName, dateMin, dateMax, abtestId, abtestType)

	buffer, err := file.WriteToBuffer()
	if err != nil {
		return "", err
	}
	bytes := buffer.Bytes()

	c.Writer.WriteHeader(http.StatusOK)
	c.Header("Content-Disposition", fmt.Sprintf("attachment; filename=%v", fileName))
	c.Header("Content-Type", "application/octet-stream")
	c.Header("Accept-Length", fmt.Sprintf("%d", len(bytes)))
	_, err = c.Writer.Write(bytes)
	if err != nil {
		return "", err
	}

	return "success", nil
}

func generateExcelFile(list []*Info) *excelize.File {
	f := excelize.NewFile()

	sheetName := SHEET_NAME
	writeCommonCellValue(f, sheetName)

	row := int64(2)
	for _, value := range list {
		f.SetCellValue(sheetName, "A"+strconv.FormatInt(row, 10), value.Level_pos_id)
		f.SetCellValue(sheetName, "B"+strconv.FormatInt(row, 10), value.Level_version)
		f.SetCellValue(sheetName, "C"+strconv.FormatInt(row, 10), value.Enter_cnt)
		f.SetCellValue(sheetName, "D"+strconv.FormatInt(row, 10), value.Reshuffle_cnt)
		f.SetCellValue(sheetName, "E"+strconv.FormatInt(row, 10), value.Challenge_cnt)
		f.SetCellValue(sheetName, "F"+strconv.FormatInt(row, 10), value.Pass_cnt)
		f.SetCellValue(sheetName, "G"+strconv.FormatInt(row, 10), value.Fail_cnt)
		f.SetCellValue(sheetName, "H"+strconv.FormatInt(row, 10), value.Refresh_cnt)
		f.SetCellValue(sheetName, "I"+strconv.FormatInt(row, 10), value.Step_remain_cnt)
		f.SetCellValue(sheetName, "J"+strconv.FormatInt(row, 10), value.Fuuu_cnt)
		f.SetCellValue(sheetName, "K"+strconv.FormatInt(row, 10), value.Enter_user)
		f.SetCellValue(sheetName, "L"+strconv.FormatInt(row, 10), value.Pass_user)
		f.SetCellValue(sheetName, "M"+strconv.FormatInt(row, 10), value.Fail_user)
		f.SetCellValue(sheetName, "N"+strconv.FormatInt(row, 10), value.Below_2step_user)
		f.SetCellValue(sheetName, "O"+strconv.FormatInt(row, 10), value.Gold_cost_user)
		f.SetCellValue(sheetName, "P"+strconv.FormatInt(row, 10), value.Gold_cost)
		f.SetCellValue(sheetName, "Q"+strconv.FormatInt(row, 10), value.Movement_cost)
		f.SetCellValue(sheetName, "R"+strconv.FormatInt(row, 10), value.Combo_cost)
		f.SetCellValue(sheetName, "S"+strconv.FormatInt(row, 10), value.Rainbow_cost)
		f.SetCellValue(sheetName, "T"+strconv.FormatInt(row, 10), value.Step3_cost)
		f.SetCellValue(sheetName, "U"+strconv.FormatInt(row, 10), value.Hammer_cost)
		f.SetCellValue(sheetName, "V"+strconv.FormatInt(row, 10), value.Exchange_cost)
		f.SetCellValue(sheetName, "W"+strconv.FormatInt(row, 10), value.Crossitem_cost)
		f.SetCellValue(sheetName, "X"+strconv.FormatInt(row, 10), value.Item_cost)
		f.SetCellValue(sheetName, "Y"+strconv.FormatInt(row, 10), value.Difficulty_rate_total)
		f.SetCellValue(sheetName, "Z"+strconv.FormatInt(row, 10), value.Fuuu_total)
		f.SetCellValue(sheetName, "AA"+strconv.FormatInt(row, 10), value.Step_remain_total)
		f.SetCellValue(sheetName, "AB"+strconv.FormatInt(row, 10), value.Gold_total)
		f.SetCellValue(sheetName, "AC"+strconv.FormatInt(row, 10), value.Goldrate_total)
		f.SetCellValue(sheetName, "AD"+strconv.FormatInt(row, 10), value.Item_per_total)
		f.SetCellValue(sheetName, "AE"+strconv.FormatInt(row, 10), value.Movement_per_total)
		f.SetCellValue(sheetName, "AF"+strconv.FormatInt(row, 10), value.Combo_per_total)
		f.SetCellValue(sheetName, "AG"+strconv.FormatInt(row, 10), value.Rainbow_per_total)
		f.SetCellValue(sheetName, "AH"+strconv.FormatInt(row, 10), value.Step3_per_total)
		f.SetCellValue(sheetName, "AI"+strconv.FormatInt(row, 10), value.Hammer_per_total)
		f.SetCellValue(sheetName, "AJ"+strconv.FormatInt(row, 10), value.Exchange_per_total)
		f.SetCellValue(sheetName, "AK"+strconv.FormatInt(row, 10), value.Crossitem_per_total)
		f.SetCellValue(sheetName, "AL"+strconv.FormatInt(row, 10), value.Refresh_per_total)
		f.SetCellValue(sheetName, "AM"+strconv.FormatInt(row, 10), value.Below_2step_rate)
		f.SetCellValue(sheetName, "AN"+strconv.FormatInt(row, 10), value.Top10percent)
		f.SetCellValue(sheetName, "AO"+strconv.FormatInt(row, 10), value.Top20percent)
		f.SetCellValue(sheetName, "AP"+strconv.FormatInt(row, 10), value.Top30percent)
		f.SetCellValue(sheetName, "AQ"+strconv.FormatInt(row, 10), value.Top50percent)
		f.SetCellValue(sheetName, "AR"+strconv.FormatInt(row, 10), value.Top75percent)
		f.SetCellValue(sheetName, "AS"+strconv.FormatInt(row, 10), value.Liushi_user)
		f.SetCellValue(sheetName, "AT"+strconv.FormatInt(row, 10), value.Liushilv_total)
		row++
	}

	return f
}

func writeCommonCellValue(f *excelize.File, sheetName string) {
	f.SetCellValue(sheetName, "A1", "level_pos_id")
	f.SetCellValue(sheetName, "B1", "level_version")
	f.SetCellValue(sheetName, "C1", "enter_cnt")
	f.SetCellValue(sheetName, "D1", "reshuffle_cnt")
	f.SetCellValue(sheetName, "E1", "challenge_cnt")
	f.SetCellValue(sheetName, "F1", "pass_cnt")
	f.SetCellValue(sheetName, "G1", "fail_cnt")
	f.SetCellValue(sheetName, "H1", "refresh_cnt")
	f.SetCellValue(sheetName, "I1", "step_remain_cnt")
	f.SetCellValue(sheetName, "J1", "fuuu_cnt")
	f.SetCellValue(sheetName, "K1", "enter_user")
	f.SetCellValue(sheetName, "L1", "pass_user")
	f.SetCellValue(sheetName, "M1", "fail_user")
	f.SetCellValue(sheetName, "N1", "below_2step_user")
	f.SetCellValue(sheetName, "O1", "gold_cost_user")
	f.SetCellValue(sheetName, "P1", "gold_cost")
	f.SetCellValue(sheetName, "Q1", "movement_cost")
	f.SetCellValue(sheetName, "R1", "combo_cost")
	f.SetCellValue(sheetName, "S1", "rainbow_cost")
	f.SetCellValue(sheetName, "T1", "step3_cost")
	f.SetCellValue(sheetName, "U1", "hammer_cost")
	f.SetCellValue(sheetName, "V1", "exchange_cost")
	f.SetCellValue(sheetName, "W1", "crossitem_cost")
	f.SetCellValue(sheetName, "X1", "item_cost")
	f.SetCellValue(sheetName, "Y1", "difficulty_rate_total")
	f.SetCellValue(sheetName, "Z1", "fuuu_total")
	f.SetCellValue(sheetName, "AA1", "step_remain_total")
	f.SetCellValue(sheetName, "AB1", "gold_total")
	f.SetCellValue(sheetName, "AC1", "goldrate_total")
	f.SetCellValue(sheetName, "AD1", "item_per_total")
	f.SetCellValue(sheetName, "AE1", "movement_per_total")
	f.SetCellValue(sheetName, "AF1", "combo_per_total")
	f.SetCellValue(sheetName, "AG1", "rainbow_per_total")
	f.SetCellValue(sheetName, "AH1", "step3_per_total")
	f.SetCellValue(sheetName, "AI1", "hammer_per_total")
	f.SetCellValue(sheetName, "AJ1", "exchange_per_total")
	f.SetCellValue(sheetName, "AK1", "crossitem_per_total")
	f.SetCellValue(sheetName, "AL1", "refresh_per_total")
	f.SetCellValue(sheetName, "AM1", "below_2step_rate")
	f.SetCellValue(sheetName, "AN1", "top10percent")
	f.SetCellValue(sheetName, "AO1", "top20percent")
	f.SetCellValue(sheetName, "AP1", "top30percent")
	f.SetCellValue(sheetName, "AQ1", "top50percent")
	f.SetCellValue(sheetName, "AR1", "top75percent")
	f.SetCellValue(sheetName, "AS1", "liushi_user")
	f.SetCellValue(sheetName, "AT1", "liushilv_total")
}

func loadInfoInner(c *gin.Context) ([]*Info, error) {
	levelMin, err := getLevel(c, "level_min")
	if err != nil {
		return nil, err
	}

	levelMax, err := getLevel(c, "level_max")
	if err != nil {
		return nil, err
	}

	dateMin, err := getDate(c, "date_min")
	if err != nil {
		return nil, err
	}

	dateMax, err := getDate(c, "date_max")
	if err != nil {
		return nil, err
	}

	abTestId := ""
	strAbTestId := c.Query("abtest_id")
	if len(strAbTestId) > 0 {
		abTestId = strAbTestId
	}

	abTestType := ""
	strAbTestType := c.Query("abtest_type")
	if len(strAbTestType) > 0 {
		abTestType = strAbTestType
	}

	infoList, err := loadInfo(levelMin, levelMax, dateMin, dateMax, abTestId, abTestType)
	if err != nil {
		return nil, err
	}
	return infoList, err
}

func getLevel(c *gin.Context, key string) (int32, error) {
	keyValue := c.Query(key)
	if keyValue == "" {
		return 0, fmt.Errorf("key:%v is empty!", key)
	}

	levelMin, err := strconv.ParseInt(keyValue, 10, 32)
	if err != nil {
		return 0, err
	}
	return int32(levelMin), nil
}

func getDate(c *gin.Context, key string) (string, error) {
	keyValue := c.Query(key)
	if keyValue == "" {
		return "", fmt.Errorf("key:%v is empty!", key)
	}

	date, err := time.Parse(LAYOUT_YEAR_MONTH_DAY, keyValue)
	if err != nil {
		return "", err
	}
	return date.Format(LAYOUT_YEAR_MONTH_DAY), nil
}
