package v1

import (
	"context"
	"github.com/jackc/pgx/v4"
	"server/server/config"
	"strconv"
	"strings"
)

type Info struct {
	Level_pos_id          int64  `db:"level_pos_id" json:"level_pos_id"`
	Level_version         int64  `db:"level_version" json:"level_version"`
	Enter_cnt             int64  `db:"enter_cnt" json:"enter_cnt"`
	Reshuffle_cnt         int64  `db:"reshuffle_cnt" json:"reshuffle_cnt"`
	Challenge_cnt         int64  `db:"challenge_cnt" json:"challenge_cnt"`
	Pass_cnt              int64  `db:"pass_cnt" json:"pass_cnt"`
	Fail_cnt              int64  `db:"fail_cnt" json:"fail_cnt"`
	Refresh_cnt           int64  `db:"refresh_cnt" json:"refresh_cnt"`
	Step_remain_cnt       int64  `db:"step_remain_cnt" json:"step_remain_cnt"`
	Fuuu_cnt              int64  `db:"fuuu_cnt" json:"fuuu_cnt"`
	Enter_user            int64  `db:"enter_user" json:"enter_user"`
	Pass_user             int64  `db:"pass_user" json:"pass_user"`
	Fail_user             int64  `db:"fail_user" json:"fail_user"`
	Below_2step_user      int64  `db:"below_2step_user" json:"below_2_step_user"`
	Gold_cost_user        int64  `db:"gold_cost_user" json:"gold_cost_user"`
	Gold_cost             int64  `db:"gold_cost" json:"gold_cost"`
	Movement_cost         int64  `db:"movement_cost" json:"movement_cost"`
	Combo_cost            int64  `db:"combo_cost" json:"combo_cost"`
	Rainbow_cost          int64  `db:"rainbow_cost" json:"rainbow_cost"`
	Step3_cost            int64  `db:"step3_cost" json:"step_3_cost"`
	Hammer_cost           int64  `db:"hammer_cost" json:"hammer_cost"`
	Exchange_cost         int64  `db:"exchange_cost" json:"exchange_cost"`
	Crossitem_cost        int64  `db:"crossitem_cost" json:"crossitem_cost"`
	Item_cost             int64  `db:"item_cost" json:"item_cost"`
	Difficulty_rate_total string `db:"difficulty_rate_total" json:"difficulty_rate_total"`
	Fuuu_total            string `db:"fuuu_total" json:"fuuu_total"`
	Step_remain_total     string `db:"step_remain_total" json:"step_remain_total"`
	Gold_total            string `db:"gold_total" json:"gold_total"`
	Goldrate_total        string `db:"goldrate_total" json:"goldrate_total"`
	Item_per_total        string `db:"item_per_total" json:"item_per_total"`
	Movement_per_total    string `db:"movement_per_total" json:"movement_per_total"`
	Combo_per_total       string `db:"combo_per_total" json:"combo_per_total"`
	Rainbow_per_total     string `db:"rainbow_per_total" json:"rainbow_per_total"`
	Step3_per_total       string `db:"step3_per_total" json:"step_3_per_total"`
	Hammer_per_total      string `db:"hammer_per_total" json:"hammer_per_total"`
	Exchange_per_total    string `db:"exchange_per_total" json:"exchange_per_total"`
	Crossitem_per_total   string `db:"crossitem_per_total" json:"crossitem_per_total"`
	Refresh_per_total     string `db:"refresh_per_total" json:"refresh_per_total"`
	Below_2step_rate      string `db:"below_2step_rate" json:"below_2_step_rate"`
	Top10percent          string `db:"top10percent" json:"top_10_percent"`
	Top20percent          string `db:"top20percent" json:"top_20_percent"`
	Top30percent          string `db:"top30percent" json:"top_30_percent"`
	Top50percent          string `db:"top50percent" json:"top_50_percent"`
	Top75percent          string `db:"top75percent" json:"top_75_percent"`
	Liushi_user           int64  `db:"liushi_user" json:"liushi_user"`
	Liushilv_total        string `db:"liushilv_total" json:"liushilv_total"`
}

const (
	sql = "" +
		" SELECT a.level_pos_id                                                                                  as level_pos_id,          " +
		"        a.level_version                                                                                 as level_version,         " +
		"        coalesce(sum(a.enter_cnt), 0)                                                                   as enter_cnt,             " +
		"        coalesce(sum(a.reshuffle_cnt), 0)                                                               as reshuffle_cnt,         " +
		"        coalesce(sum(a.challenge_cnt), 0)                                                               as challenge_cnt,         " +
		"        coalesce(sum(a.pass_cnt), 0)                                                                    as pass_cnt,              " +
		"        coalesce(sum(a.fail_cnt), 0)                                                                    as fail_cnt,              " +
		"        coalesce(sum(a.refresh_cnt), 0)                                                                 as refresh_cnt,           " +
		"        coalesce(sum(a.step_remain_cnt), 0)                                                             as step_remain_cnt,       " +
		"        coalesce(sum(a.fuuu_cnt), 0)                                                                    as fuuu_cnt,              " +
		"        coalesce(sum(a.enter_user), 0)                                                                  as enter_user,            " +
		"        coalesce(sum(a.pass_user), 0)                                                                   as pass_user,             " +
		"        coalesce(sum(a.fail_user), 0)                                                                   as fail_user,             " +
		"        coalesce(sum(a.below_2step_user), 0)                                                            as below_2step_user,      " +
		"        coalesce(sum(a.gold_cost_user), 0)                                                              as gold_cost_user,        " +
		"        coalesce(sum(a.gold_cost), 0)                                                                   as gold_cost,             " +
		"        coalesce(sum(a.movement_cost), 0)                                                               as movement_cost,         " +
		"        coalesce(sum(a.combo_cost), 0)                                                                  as combo_cost,            " +
		"        coalesce(sum(a.rainbow_cost), 0)                                                                as rainbow_cost,          " +
		"        coalesce(sum(a.step3_cost), 0)                                                                  as step3_cost,            " +
		"        coalesce(sum(a.hammer_cost), 0)                                                                 as hammer_cost,           " +
		"        coalesce(sum(a.exchange_cost), 0)                                                               as exchange_cost,         " +
		"        coalesce(sum(a.crossitem_cost), 0)                                                              as crossitem_cost,        " +
		"        coalesce(sum(a.item_cost), 0)                                                                   as item_cost,             " +
		"        case                                                                                                                      " +
		"            when sum(a.pass_cnt) = 0 then '0.00'                                                                                  " +
		"            when sum(a.pass_cnt) > 0                                                                                              " +
		"                then cast(cast(                                                                                                   " +
		"                    sum(a.challenge_cnt) / sum(a.pass_cnt)::float as decimal(18, 2)) as varchar) end    as difficulty_rate_total, " +
		"        case                                                                                                                      " +
		"            when sum(a.fuuu_cnt) = 0 then '0.00'                                                                                  " +
		"            when sum(a.fuuu_cnt) > 0                                                                                              " +
		"                then cast(cast(                                                                                                   " +
		"                    sum(a.fail_cnt) / sum(a.fuuu_cnt)::float as decimal(18, 2)) as varchar) end         as fuuu_total,            " +
		"        case                                                                                                                      " +
		"            when sum(a.pass_user) = 0 then '0.00'                                                                                 " +
		"            when sum(a.pass_user) > 0                                                                                             " +
		"                then cast(cast(                                                                                                   " +
		"                    sum(a.step_remain_cnt) / sum(a.pass_user)::float as decimal(18, 2)) as varchar) end as step_remain_total,     " +
		"        case                                                                                                                      " +
		"            when sum(a.pass_user) = 0 then '0.00'                                                                                 " +
		"            when sum(a.pass_user) > 0                                                                                             " +
		"                then cast(cast(                                                                                                   " +
		"                    sum(a.gold_cost) / sum(a.pass_user)::float as decimal(18, 2)) as varchar) end       as gold_total,            " +
		"        case                                                                                                                      " +
		"            when sum(a.pass_user) = 0 then '0.00%'                                                                                " +
		"            when sum(a.pass_user) > 0                                                                                             " +
		"                then cast(cast(sum(a.gold_cost_user) * 100 / sum(a.pass_user) ::float as decimal(18, 2)) as varchar) ||           " +
		"                     '%' end                                                                            as goldrate_total,        " +
		"        case                                                                                                                      " +
		"            when sum(a.pass_user) = 0 then '0.00'                                                                                 " +
		"            when sum(a.pass_user) > 0                                                                                             " +
		"                then cast(cast(sum(a.item_cost) / sum(a.pass_user)::float as decimal(18, 2)) as varchar)                          " +
		"            end                                                                                         as item_per_total,        " +
		"        case                                                                                                                      " +
		"            when sum(a.pass_user) = 0 then '0.00'                                                                                 " +
		"            when sum(a.pass_user) > 0                                                                                             " +
		"                then cast(cast(sum(a.movement_cost) / sum(a.pass_user)::float as decimal(18, 2)) as varchar)                      " +
		"            end                                                                                         as movement_per_total,    " +
		"        case                                                                                                                      " +
		"            when sum(a.pass_user) = 0 then '0.00'                                                                                 " +
		"            when sum(a.pass_user) > 0                                                                                             " +
		"                then cast(cast(sum(a.combo_cost) / sum(a.pass_user)::float as decimal(18, 2)) as varchar)                         " +
		"            end                                                                                         as combo_per_total,       " +
		"        case                                                                                                                      " +
		"            when sum(a.pass_user) = 0 then '0.00'                                                                                 " +
		"            when sum(a.pass_user) > 0                                                                                             " +
		"                then cast(cast(sum(a.rainbow_cost) / sum(a.pass_user)::float as decimal(18, 2)) as varchar)                       " +
		"            end                                                                                         as rainbow_per_total,     " +
		"        case                                                                                                                      " +
		"            when sum(a.pass_user) = 0 then '0.00'                                                                                 " +
		"            when sum(a.pass_user) > 0                                                                                             " +
		"                then cast(cast(sum(a.step3_cost) / sum(a.pass_user)::float as decimal(18, 2)) as varchar)                         " +
		"            end                                                                                         as step3_per_total,       " +
		"        case                                                                                                                      " +
		"            when sum(a.pass_user) = 0 then '0.00'                                                                                 " +
		"            when sum(a.pass_user) > 0                                                                                             " +
		"                then cast(cast(sum(a.hammer_cost) / sum(a.pass_user)::float as decimal(18, 2)) as varchar)                        " +
		"            end                                                                                         as hammer_per_total,      " +
		"        case                                                                                                                      " +
		"            when sum(a.pass_user) = 0 then '0.00'                                                                                 " +
		"            when sum(a.pass_user) > 0                                                                                             " +
		"                then cast(cast(sum(a.exchange_cost) / sum(a.pass_user)::float as decimal(18, 2)) as varchar)                      " +
		"            end                                                                                         as exchange_per_total,    " +
		"        case                                                                                                                      " +
		"            when sum(a.pass_user) = 0 then '0.00'                                                                                 " +
		"            when sum(a.pass_user) > 0                                                                                             " +
		"                then cast(cast(sum(a.crossitem_cost) / sum(a.pass_user)::float as decimal(18, 2)) as varchar)                     " +
		"            end                                                                                         as crossitem_per_total,   " +
		"        case                                                                                                                      " +
		"            when sum(a.pass_user) = 0 then '0.00'                                                                                 " +
		"            when sum(a.pass_user) > 0                                                                                             " +
		"                then cast(cast(sum(a.refresh_cnt) / sum(a.pass_user)::float as decimal(18, 2)) as varchar)                        " +
		"            end                                                                                         as refresh_per_total,     " +
		"        case                                                                                                                      " +
		"            when sum(a.pass_user) = 0 then '0.00%'                                                                                " +
		"            when sum(a.pass_user) > 0                                                                                             " +
		"                then cast(cast(sum(a.below_2step_user) * 100 / sum(a.pass_user)::float as decimal(18, 2)) as varchar)             " +
		"                || '%'                                                                                                            " +
		"            end                                                                                         as below_2step_rate,      " +
		"        case                                                                                                                      " +
		"            when avg(a.top10percent) is null then '-'                                                                             " +
		"            else cast(avg(a.top10percent) as varchar) end                                                                         " +
		"                                                                                                        as top10percent,          " +
		"        case                                                                                                                      " +
		"            when avg(a.top20percent) is null then '-'                                                                             " +
		"            else cast(avg(a.top20percent) as varchar) end                                               as top20percent,          " +
		"        case                                                                                                                      " +
		"            when avg(a.top30percent) is null then '-'                                                                             " +
		"            else cast(avg(a.top30percent) as varchar) end                                               as top30percent,          " +
		"        case                                                                                                                      " +
		"            when avg(a.top50percent) is null then '-'                                                                             " +
		"            else cast(avg(a.top50percent) as varchar) end                                               as top50percent,          " +
		"        case                                                                                                                      " +
		"            when avg(a.top75percent) is null then '-'                                                                             " +
		"            else cast(avg(a.top75percent) as varchar) end                                               as top75percent,          " +
		"        sum(coalesce(a.liushi_user, 0))                                                                 as liushi_user,           " +
		"        case                                                                                                                      " +
		"            when sum(coalesce(a.pass_user, 0)) = 0 then '0.00%'                                                                   " +
		"            when sum(coalesce(a.pass_user, 0)) > 0                                                                                " +
		"                then cast(cast(sum(coalesce(a.liushi_user, 0)) * 100 /                                                            " +
		"                               sum(coalesce(a.pass_user, 0))::float as decimal(18, 2)) as varchar)                                " +
		"                || '%'                                                                                                            " +
		"            end                                                                                         as liushilv_total         " +
		"                                                                                                                                  " +
		" from (select *                                                                                                                   " +
		"       from designisland_bi.f_level_analysis_data b                                                                           " +
		"       where #### ) a                                                                                             " +
		" group by 1, 2;                                                                                                             "
)

func loadInfo(levelMin int32, levelMax int32, dateMin string, dateMax string, abTestId string, abTestType string) ([]*Info, error) {
	conn, err := pgx.Connect(context.Background(), config.Config.LevelAnalysis.RedshiftUrl)
	if err != nil {
		return make([]*Info, 0), err
	}
	defer func() {
		if conn != nil {
			_ = conn.Close(context.Background())
		}
	}()

	sql, sqlParams := generateSqlAndParam(levelMin, levelMax, dateMin, dateMax, abTestId, abTestType)
	row, err := conn.Query(context.Background(), sql, sqlParams...)
	if err != nil {
		return make([]*Info, 0), err
	}

	infos := make([]*Info, 0)
	for row.Next() {
		info, err := scan(row)
		if err != nil {
			return make([]*Info, 0), err
		}
		infos = append(infos, info)
	}
	return infos, nil
}

func generateSqlAndParam(levelMin int32, levelMax int32, dateMin string, dateMax string, abTestId string, abTestType string) (string, []interface{}) {
	base := "b.ev_date >= date($1)                                                                                      " +
		"         and b.ev_date < date($2)                                                                                       " +
		"         and b.level_pos_id >= $3                                                                                                  " +
		"         and b.level_pos_id < $4 "

	params := make([]interface{}, 0)
	params = append(params, dateMin)
	params = append(params, dateMax)
	params = append(params, levelMin)
	params = append(params, levelMax)

	count := int64(5)
	if len(abTestId) > 0 {
		base += " and b.abtest_id = $" + strconv.FormatInt(count, 10)
		params = append(params, abTestId)
		count++
	}

	if len(abTestType) > 0 {
		base += " and b.abtest_type = $" + strconv.FormatInt(count, 10)
		params = append(params, abTestType)
		count++
	}

	return strings.ReplaceAll(sql, "####", base), params
}

func scan(row pgx.Rows) (*Info, error) {
	var level_pos_id int64
	var level_version int64
	var enter_cnt int64
	var reshuffle_cnt int64
	var challenge_cnt int64
	var pass_cnt int64
	var fail_cnt int64
	var refresh_cnt int64
	var step_remain_cnt int64
	var fuuu_cnt int64
	var enter_user int64
	var pass_user int64
	var fail_user int64
	var below_2step_user int64
	var gold_cost_user int64
	var gold_cost int64
	var movement_cost int64
	var combo_cost int64
	var rainbow_cost int64
	var step3_cost int64
	var hammer_cost int64
	var exchange_cost int64
	var crossitem_cost int64
	var item_cost int64
	var difficulty_rate_total string
	var fuuu_total string
	var step_remain_total string
	var gold_total string
	var goldrate_total string
	var item_per_total string
	var movement_per_total string
	var combo_per_total string
	var rainbow_per_total string
	var step3_per_total string
	var hammer_per_total string
	var exchange_per_total string
	var crossitem_per_total string
	var refresh_per_total string
	var below_2step_rate string
	var top10percent string
	var top20percent string
	var top30percent string
	var top50percent string
	var top75percent string
	var liushi_user int64
	var liushilv_total string
	err := row.Scan(&level_pos_id,
		&level_version,
		&enter_cnt,
		&reshuffle_cnt,
		&challenge_cnt,
		&pass_cnt,
		&fail_cnt,
		&refresh_cnt,
		&step_remain_cnt,
		&fuuu_cnt,
		&enter_user,
		&pass_user,
		&fail_user,
		&below_2step_user,
		&gold_cost_user,
		&gold_cost,
		&movement_cost,
		&combo_cost,
		&rainbow_cost,
		&step3_cost,
		&hammer_cost,
		&exchange_cost,
		&crossitem_cost,
		&item_cost,
		&difficulty_rate_total,
		&fuuu_total,
		&step_remain_total,
		&gold_total,
		&goldrate_total,
		&item_per_total,
		&movement_per_total,
		&combo_per_total,
		&rainbow_per_total,
		&step3_per_total,
		&hammer_per_total,
		&exchange_per_total,
		&crossitem_per_total,
		&refresh_per_total,
		&below_2step_rate,
		&top10percent,
		&top20percent,
		&top30percent,
		&top50percent,
		&top75percent,
		&liushi_user,
		&liushilv_total)
	if err != nil {
		return nil, err
	}
	return &Info{
		Level_pos_id:          level_pos_id,
		Level_version:         level_version,
		Enter_cnt:             enter_cnt,
		Reshuffle_cnt:         reshuffle_cnt,
		Challenge_cnt:         challenge_cnt,
		Pass_cnt:              pass_cnt,
		Fail_cnt:              fail_cnt,
		Refresh_cnt:           refresh_cnt,
		Step_remain_cnt:       step_remain_cnt,
		Fuuu_cnt:              fuuu_cnt,
		Enter_user:            enter_user,
		Pass_user:             pass_user,
		Fail_user:             fail_user,
		Below_2step_user:      below_2step_user,
		Gold_cost_user:        gold_cost_user,
		Gold_cost:             gold_cost,
		Movement_cost:         movement_cost,
		Combo_cost:            combo_cost,
		Rainbow_cost:          rainbow_cost,
		Step3_cost:            step3_cost,
		Hammer_cost:           hammer_cost,
		Exchange_cost:         exchange_cost,
		Crossitem_cost:        crossitem_cost,
		Item_cost:             item_cost,
		Difficulty_rate_total: difficulty_rate_total,
		Fuuu_total:            fuuu_total,
		Step_remain_total:     step_remain_total,
		Gold_total:            gold_total,
		Goldrate_total:        goldrate_total,
		Item_per_total:        item_per_total,
		Movement_per_total:    movement_per_total,
		Combo_per_total:       combo_per_total,
		Rainbow_per_total:     rainbow_per_total,
		Step3_per_total:       step3_per_total,
		Hammer_per_total:      hammer_per_total,
		Exchange_per_total:    exchange_per_total,
		Crossitem_per_total:   crossitem_per_total,
		Refresh_per_total:     refresh_per_total,
		Below_2step_rate:      below_2step_rate,
		Top10percent:          top10percent,
		Top20percent:          top20percent,
		Top30percent:          top30percent,
		Top50percent:          top50percent,
		Top75percent:          top75percent,
		Liushi_user:           liushi_user,
		Liushilv_total:        liushilv_total,
	}, nil

}
