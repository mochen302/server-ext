package server

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"server/server/common/log"
	"server/server/config"
	"server/server/levelanalysis/v1"
	v2 "server/server/levelanalysis/v2"
	"server/server/monitor"
	"server/server/restart"
)

type BUSINESS_CODE int8

const (
	SUCCESS        BUSINESS_CODE = 0
	EXCEPTION      BUSINESS_CODE = -1
	TIME_NOT_VALID BUSINESS_CODE = -2
)

func Router(r *gin.Engine, m *monitor.MonitorInterfaceInfo) {
	r.GET("/restart", func(c *gin.Context) {
		restart.Do()
		setResponse(c, http.StatusOK, SUCCESS, "")
	})

	//r.GET("/health", func(c *gin.Context) {
	//	setResponse(c, http.StatusOK, SUCCESS, "ok")
	//})

	r.GET("/monitor/time/get", func(c *gin.Context) {
		handleInternal(c, m.GetCurrentTime)
	})

	r.GET("/monitor/time/reset", func(c *gin.Context) {
		handleInternal(c, m.ResetCurrentTime, config.Config.Monitor.Cmd)
	})

	r.GET("/monitor/time/set", func(c *gin.Context) {
		strTime := c.Query("time")
		if strTime == "" {
			setResponse(c, http.StatusOK, TIME_NOT_VALID, fmt.Sprintf("time:%v is not ok!", strTime))
			return
		}
		handleInternal(c, m.SetCurrentTime, strTime, config.Config.Monitor.Cmd)
	})

	r.GET("/ll/load", func(c *gin.Context) {
		infos, err := v1.Load(c)
		if err != nil {
			log.Error("levelanalysis-load err:", err)
			setResponse(c, http.StatusInternalServerError, EXCEPTION, err.Error())
		} else {
			setResponse(c, http.StatusOK, SUCCESS, infos)
		}
	})

	r.GET("/ll/export", func(c *gin.Context) {
		_, err := v1.Export(c)
		if err != nil {
			log.Error("levelanalysis-export err:", err)
			setResponse(c, http.StatusInternalServerError, EXCEPTION, err.Error())
		}
	})

	r.GET("/ll/v2/load", func(c *gin.Context) {
		infos, err := v2.Load(c)
		if err != nil {
			log.Error("levelanalysis-v2-load err:", err)
			setResponse(c, http.StatusInternalServerError, EXCEPTION, err.Error())
		} else {
			setResponse(c, http.StatusOK, SUCCESS, infos)
		}
	})

	r.GET("/ll/v2/export", func(c *gin.Context) {
		_, err := v2.Export(c)
		if err != nil {
			log.Error("levelanalysis-v2-export err:", err)
			setResponse(c, http.StatusInternalServerError, EXCEPTION, err.Error())
		}
	})
}

func handleInternal(c *gin.Context, f func(p ...interface{}) (result interface{}), param ...interface{}) {
	defer func() {
		err := recover()
		if err != nil {
			log.Error(err)
			setResponse(c, http.StatusInternalServerError, EXCEPTION, "internal error")
		}
	}()

	result := f(param)
	setResponse(c, http.StatusOK, SUCCESS, fmt.Sprint(result))
}

func setResponse(c *gin.Context, code int, businessCode BUSINESS_CODE, result interface{}) {
	c.JSON(code, gin.H{
		"ret":  businessCode,
		"info": result,
	})
}
