package db

import (
	"context"
	"github.com/jackc/pgx/v4"
	"server/server/common/log"
	"server/server/config"
	"sync"
)

var once sync.Once
var DbConn *pgx.Conn

func Start() {
	conn, err := pgx.Connect(context.Background(), config.Config.LevelAnalysis.RedshiftUrl)
	if err != nil {
		panic(err)
	}
	DbConn = conn
}

func Close() {
	if DbConn != nil {
		err := DbConn.Close(context.Background())
		if err != nil {
			log.Error("close redshift err:", err)
		}
	}
}
