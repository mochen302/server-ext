package db

import (
	"bufio"
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"os"
	"testing"
	"time"
)

func TestRedShift(t *testing.T) {
	url := "host=designisland-bi.cdc2g8od5ghp.us-west-2.redshift.amazonaws.com port=5439 database=prod user= di_readonly password=di_readonly_BI#888"

	conn, err := pgx.Connect(context.Background(), url)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		os.Exit(1)
	}
	defer conn.Close(context.Background())

	var startDate time.Time
	var endDate time.Time
	err = conn.QueryRow(context.Background(), "select start_date,end_date from designisland_bi.f_level_analysis_date").Scan(&startDate, &endDate)
	if err != nil {
		fmt.Fprintf(os.Stderr, "QueryRow failed: %v\n", err)
		os.Exit(1)
	}
	fmt.Println(startDate, endDate)
}

func TestPg(t *testing.T) {
	url := "host=designisland-bi.cdc2g8od5ghp.us-west-2.redshift.amazonaws.com port=5439 database=prod user= di_readonly password=di_readonly_BI#888"
	db, err := sqlx.Open("postgres", url)

	if err != nil {
		fmt.Println("Open error:", err)
		panic(err)
	}
	defer db.Close()

	var startDate time.Time
	var endDate time.Time
	err = db.QueryRow("select start_date,end_date from designisland_bi.f_level_analysis_date").Scan(&startDate, &endDate)
	if err != nil {
		fmt.Fprintf(os.Stderr, "QueryRow failed: %v\n", err)
		os.Exit(1)
	}
	fmt.Println(startDate, endDate)
}

func TestGenerate(t *testing.T) {
	f := "f.SetCellValue(sheetName, \"%v\", \"\")"
	base := ""
	for i := 0; i < 26; i++ {
		fmt.Println(fmt.Sprintf(f, base+string('A'+i)))
	}
	base = "A"
	for i := 0; i < 26; i++ {
		fmt.Println(fmt.Sprintf(f, base+string('A'+i)))
	}

}

func TestExcelRead(t *testing.T) {
	temp := "insert into designisland_bi.ur_fpid_200604 values "
	fmt.Println(temp)

	file, err := os.Open("test.txt")
	if err != nil {
		return
	}
	defer file.Close()

	newFile, err := os.Create("test-new.txt")
	if err != nil {
		return
	}
	newFile.WriteString(temp + "\r\n")

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		text := scanner.Text()
		//fields := strings.Fields()
		//param1, param2 := "", ""
		//for _, value := range fields {
		//	if value != "" {
		//		if param1 == "" {
		//			param1 = value
		//			continue
		//		}
		//		if param2 == "" {
		//			param2 = value
		//		}
		//	}
		//}
		////newFile.WriteString(fmt.Sprintf("('%v','%v'),\r\n", param1, param2))
		newFile.WriteString(fmt.Sprintf("('%v'),\r\n", text))
	}

}
