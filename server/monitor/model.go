package monitor

import (
	"bytes"
	"github.com/beevik/ntp"
	"os/exec"
	"server/server/common/log"
	"strings"
	"time"
)

const (
	LAYOUT      = "2006-01-02 15:04:05"
	LAYOUT_MORE = "2006-01-02-15:04:05"
)

var MonitorInterface MonitorInterfaceInfo

type MonitorInterfaceInfo struct {
}

func (*MonitorInterfaceInfo) GetCurrentTime(p ...interface{}) interface{} {
	timeStr, _ := time.Parse(LAYOUT, time.Now().Format(LAYOUT))
	return timeStr
}

func (*MonitorInterfaceInfo) ResetCurrentTime(p ...interface{}) interface{} {
	p0 := p[0].([]interface{})
	cmdArr := p0[0].([]string)

	response, err := ntp.Time("0.beevik-ntp.pool.ntp.org")
	if err != nil {
		panic(err)
	}

	response = response.UTC()

	setCurrentTime0(response, cmdArr)
	return true
}

func (*MonitorInterfaceInfo) SetCurrentTime(p ...interface{}) interface{} {
	p0 := p[0].([]interface{})
	timeKey := p0[0].(string)
	cmdArr := p0[1].([]string)

	parse, _ := time.Parse(LAYOUT_MORE, timeKey)
	setCurrentTime0(parse, cmdArr)
	return true
}

func setCurrentTime0(t time.Time, cmdArr []string) interface{} {
	timeKey := t.Format(LAYOUT)

	for _, cmdNow := range cmdArr {
		cmdFinal := strings.ReplaceAll(cmdNow, "#date#", timeKey)
		execCmd(cmdFinal)
	}

	return true
}

func execCmd(cmdName string) {

	cmd := exec.Command("/bin/bash", "-c", cmdName)

	var out bytes.Buffer
	cmd.Stdout = &out

	err := cmd.Run()
	if err != nil {
		log.Error("exec cmd:", cmd, " ReadAll error:", err)
		return
	}

	log.Info("exec cmd:", cmd, " result:", out.String())
}
