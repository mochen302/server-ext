package restart

import (
	"github.com/cloudflare/tableflip"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func Do() {
	upg, _ := tableflip.New(tableflip.Options{})
	defer upg.Stop()

	go func() {
		sig := make(chan os.Signal, 1)
		signal.Notify(sig, syscall.SIGHUP)
		for range sig {
			upg.Upgrade()
		}
	}()

	// Listen must be called before Ready
	ln, _ := upg.Listen("tcp", "localhost:8080")
	defer ln.Close()

	go http.Serve(ln, nil)

	if err := upg.Ready(); err != nil {
		panic(err)
	}

	<-upg.Exit()
}
